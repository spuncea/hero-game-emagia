<?php

use Emagia\Utils\TerminalColors;
use PHPUnit\Framework\TestCase;

/**
 * @covers Emagia\Utils\TerminalColors
 */
class TerminalColorsTest extends TestCase
{
    use TerminalColors;

    public function testGreenText()
    {
        $this->assertSame("\033[32mhello\033[0m", $this->greenText('hello'));
    }

    public function testOrangeText()
    {
        $this->assertSame("\033[33mworld\033[0m", $this->orangeText('world'));
    }

    public function testRedText()
    {
        $this->assertSame("\033[31mtext\033[0m", $this->redText('text'));
    }

    public function testFlashingRedText()
    {
        $this->assertSame("\033[5;31mexample\033[0m", $this->flashingRedText('example'));
    }
}
