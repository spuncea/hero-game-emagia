<?php

use Emagia\BattleGroundHelper;
use Emagia\BattleGround;
use Emagia\Player\Player;
use Emagia\Utils\Config;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Emagia\BattleGround
 * @covers \Emagia\Player\Player
 * @covers \Emagia\BattleGroundHelper
 * @covers \Emagia\Skill\MagicShield
 * @covers \Emagia\Skill\RapidStrike
 */
class BattleGroundTest extends TestCase
{
    

    

    public function testStartMethod()
    {
        $battleGround = $this->getMockForAbstractClass(BattleGround::class);
        // Capture the output of the start method
        ob_start();
        $battleGround->start();
        $output = ob_get_clean();

        // Assert that the output contains certain expected strings
        $this->assertStringContainsString('STARTING ATTRIBUTES', $output);
        $this->assertStringContainsString('HERO | BEAST', $output);
        $this->assertStringContainsString('ROUND', $output);
        $this->assertStringContainsString('WINNER OF THE FIGHT IS:', $output);

    }

    
}
