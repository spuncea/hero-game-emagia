<?php

use Emagia\BattleGround;
use Emagia\BattleGroundHelper;
use Emagia\Player\Player;
use Emagia\Utils\Config;
use Emagia\Utils\TerminalColors;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Emagia\BattleGroundHelper
 * @covers \Emagia\Player\Player
 * 
 */
class BattleGroundHelperTest extends TestCase
{
    use TerminalColors;
    public function testGetHealthColor()
    {
        $helper = $this->getMockForAbstractClass(BattleGroundHelper::class);
        $this->assertEquals($this->greenText('100%'), $helper->getHealthColor(100));
        $this->assertEquals($this->orangeText('75%'), $helper->getHealthColor(75));
        $this->assertEquals($this->orangeText('50%'), $helper->getHealthColor(50));
        $this->assertEquals($this->redText('25%'), $helper->getHealthColor(25));
        $this->assertEquals($this->redText('0%'), $helper->getHealthColor(0));
    }

    public function testChooseSides()
    {
        $hero = $this->createMock(Player::class);
        $hero->expects($this->any())
            ->method('getSpeed')
            ->willReturn(10);
        $hero->expects($this->any())
            ->method('getLuck')
            ->willReturn(50);
        $hero->expects($this->once())
            ->method('setIsAttacking')
            ->with(false);
        $beast = $this->createMock(Player::class);
        $beast->expects($this->any())
            ->method('getSpeed')
            ->willReturn(10);
        $beast->expects($this->any())
            ->method('getLuck')
            ->willReturn(60);

        $beast->expects($this->once())
            ->method('setIsAttacking')
            ->with(true);
        
        // instantiate it over mocking
        $battleGroundHelper = $this->getMockForAbstractClass(BattleGroundHelper::class);
        $battleGroundHelper->chooseSides($hero, $beast);
    }

    public function testAttackKillsHero()
    {
        $hero = $this->getMockBuilder(Player::class)->getMock();
        $hero->setDefaultAttributes(Config::HERO_ATT);
        $hero->setHealth(10);
        $hero->setLuckyChance(100);
        $hero->setLuck(0); // not lucky
        $hero->setDefence(10);

        $beast = $this->getMockBuilder(Player::class)->getMock();
        $beast->setDefaultAttributes(Config::ANIMAL_ATT);
        $beast->setStrength(20);
        $beast->setHealth(100);
        $beast->setLuck(100);
        $beast->setLuckyChance(0);

        $battleGroundHelper = $this->getMockBuilder(BattleGroundHelper::class)->getMock();
        $hero->setIsAttacking(false);
        $beast->setIsDefending(false);
        $battleGroundHelper->expects($this->once())
            ->method('attack')
            ->with($hero, $beast)
            ->willReturn(true);

        $result = $battleGroundHelper->attack($hero, $beast);
        // assert that the attack method returned true indicating that the hero has died
        $this->assertTrue($result);
        // assert that the hero's health is 0
        $this->assertEquals(0, $hero->getHealth());
    }
    public function testLuckyBeastDodgesAttack()
    {
        $hero = $this->getMockBuilder(Player::class)->getMock();
        $hero->expects($this->any())
            ->method('getLuck')
            ->willReturn(0);
        $hero->expects($this->any())
            ->method('getLuckyChance')
            ->willReturn(100);

        $beast = $this->getMockBuilder(Player::class)->getMock();
        $beast->expects($this->any())
            ->method('getLuck')
            ->willReturn(100);
        $beast->expects($this->any())
            ->method('getLuckyChance')
            ->willReturn(0);
        $beastHealth = $beast->getHealth();

        $battleGround = $this->getMockBuilder(BattleGroundHelper::class)->getMock();
        $hero->setIsAttacking(true);
        $beast->setIsDefending(true);
        $battleGround->attack($hero, $beast);
        $this->assertEquals($beastHealth, $beast->getHealth());
    }
    
    public function testSwitchSides()
    {
        $abstractBattleGround = $this->getMockForAbstractClass(BattleGroundHelper::class);

        // Create two players
        $hero = $this->getMockForAbstractClass(Player::class);
        $beast = $this->getMockForAbstractClass(Player::class);

        
        // Set one player as the attacker and the other as the defender
        $hero->setIsAttacking(true);
        $beast->setIsDefending(true);
        
        // Call the switchSides method
        $abstractBattleGround->switchSides($hero, $beast);

        // Assert that the players' attacking and defending states have been switched
        $this->assertFalse($hero->isAttacking());
        $this->assertTrue($hero->isDefending());
        $this->assertTrue($beast->isAttacking());
        $this->assertFalse($beast->isDefending());
    }
    
    public function testAttackKillsBeast()
    {
        $hero = $this->getMockBuilder(Player::class)->getMock();
        $hero->setDefaultAttributes(Config::HERO_ATT);
        $hero->setStrength(20);
        $hero->setHealth(100);
        $hero->setLuck(100);
        $hero->setLuckyChance(0);

        $beast = $this->getMockBuilder(Player::class)->getMock();
        $beast->setDefaultAttributes(Config::ANIMAL_ATT);

        $beast->setHealth(10);
        $beast->setLuckyChance(100);
        $beast->setLuck(0); // not lucky
        $beast->setDefence(10);

        $battleGroundHelper = $this->getMockBuilder(BattleGroundHelper::class)->getMock();
        $hero->setIsAttacking(true);
        $beast->setIsDefending(true);

        $battleGroundHelper->expects($this->once())
            ->method('attack')
            ->with($hero, $beast)
            ->willReturn(true);

        
        $result = $battleGroundHelper->attack($hero, $beast);

        // assert that the attack method returned true indicating that the hero has died
        $this->assertTrue($result);
        // assert that the hero's health is 0
        $this->assertEquals(0, $beast->getHealth());
    }

    public function testHeroAttack()
    {
        $hero = $this->createMock(Player::class);
        $beast = $this->createMock(Player::class);
        $hero->method('isAttacking')->willReturn(true);
        $beast->method('isDefending')->willReturn(true);
        $hero->method('getStrength')->willReturn(50);
        $beast->method('getDefence')->willReturn(30);
        $hero->method('isLucky')->willReturn(false);
        $hero->expects($this->once())->method('applySkills')->with($beast);
        $battleGroundHelper = $this->getMockForAbstractClass(BattleGroundHelper::class);
        $battleGroundHelper->attack($hero, $beast);
    }

    public function testBeastAttack()
    {
        $hero = $this->createMock(Player::class);
        $beast = $this->createMock(Player::class);
        $hero->method('isDefending')->willReturn(true);
        $beast->method('isAttacking')->willReturn(true);
        $hero->method('getStrength')->willReturn(50);
        $beast->method('getDefence')->willReturn(30);
        $hero->method('isLucky')->willReturn(false);
        $hero->expects($this->once())->method('applySkills')->with($beast);
        $battleGroundHelper = $this->getMockForAbstractClass(BattleGroundHelper::class);
        $battleGroundHelper->attack($hero, $beast);
    }


    public function testShowStartUpAttributes()
    {
        $battleGround = $this->getMockBuilder(BattleGround::class)
                            ->disableOriginalConstructor()
                            ->getMockForAbstractClass();

        $hero = $this->getMockForAbstractClass(Player::class);
        $hero->setName('Orderus');
        $hero->setDefaultAttributes(Config::HERO_ATT);
        $beast = $this->getMockForAbstractClass(Player::class);
        $beast->setName('Wild Beast');
        $beast->setDefaultAttributes(Config::ANIMAL_ATT);
        $hero->setIsAttacking(true);
        $beast->setIsDefending(true);

        ob_start();
        $battleGround->showStartUpAttributes($hero, $beast);
        $output = ob_get_clean();

        $this->assertStringContainsString('STARTING ATTRIBUTES', $output);
    }

    public function testGetWinner()
    {
        // create mock instances of the Player class
        $hero = $this->createMock(Player::class);
        $beast = $this->createMock(Player::class);

        // set health and strength of hero and beast
        $hero->method('getHealth')->willReturn(80);
        $hero->method('getStrength')->willReturn(60);
        $beast->method('getHealth')->willReturn(75);
        $beast->method('getStrength')->willReturn(55);

        $battleGroundHelper = $this->getMockForAbstractClass(BattleGroundHelper::class);

        // assert that the hero is returned
        $this->assertSame($hero, $battleGroundHelper->getWinner($hero, $beast));

        // create mock instances of the Player class
        $hero = $this->createMock(Player::class);
        $beast = $this->createMock(Player::class);

        // set health and strength of hero and beast
        $hero->method('getHealth')->willReturn(40);
        $hero->method('getStrength')->willReturn(60);
        $beast->method('getHealth')->willReturn(60);
        $beast->method('getStrength')->willReturn(55);


        // assert that the hero is returned
        $this->assertSame($beast, $battleGroundHelper->getWinner($hero, $beast));

        // create mock instances of the Player class
        $hero = $this->createMock(Player::class);
        $beast = $this->createMock(Player::class);

        // set health and strength of hero and beast
        $hero->method('getHealth')->willReturn(40);
        $hero->method('getStrength')->willReturn(60);
        $beast->method('getHealth')->willReturn(40);
        $beast->method('getStrength')->willReturn(55);


        // assert that the hero is returned
        $this->assertSame($hero, $battleGroundHelper->getWinner($hero, $beast));

    }

    public function testShowBattleRoundAttributes()
    {
        $battleGround = $this->getMockBuilder(BattleGround::class)
                            ->disableOriginalConstructor()
                            ->getMockForAbstractClass();
        $hero = $this->getMockForAbstractClass(Player::class);
        $hero->setSkillApplied(null);
        $hero->setLuckyChance(0);
        $hero->setHealth(80);
        $hero->setStrength(50);
        $hero->setDefence(30);
        $hero->setSpeed(70);
        $hero->setLuck(40);
        $hero->setDamage(20);
        $hero->setIsAttacking(true);

        $beast = $this->getMockForAbstractClass(Player::class);
        $beast->setHealth(70);
        $beast->setStrength(60);
        $beast->setDefence(35);
        $beast->setSpeed(60);
        $beast->setLuck(100); // lucky beast
        $beast->setDamage(15);
        $beast->setIsAttacking(false);
        $beast->setLuckyChance(0);

        ob_start();
        $battleGround->showBattleRoundAttributes($hero, $beast, 1);
        $output = ob_get_clean();

        $this->assertStringContainsString("ROUND 1", $output);
        $this->assertStringContainsString("HERO | BEAST", $output);
    }
}