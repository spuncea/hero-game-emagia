<?php

use Emagia\Player\Player;
use Emagia\Skill\Skill;
use PHPUnit\Framework\TestCase;

/**
 * @covers Emagia\Player\Player
 */
class PlayerTest extends TestCase
{
    protected $player;
    /**
     * Set up test Player
     */
    protected function setUp(): void
    {
        $player = $this->getMockForAbstractClass(Player::class);
        
        $player->setName('Orderus');
        $player->setHealth(80);
        $player->setStrength(60);
        $player->setDefence(55);
        $player->setSpeed(65);
        $player->setLuck(25);
        $player->setLuckyChance(0);

        $this->player = $player;
    }

    public function testGetName()
    {
        $this->assertEquals('Orderus', $this->player->getName());
    }
    
    public function testGetHealth()
    {
        self::assertEquals(80, $this->player->getHealth());
    }
    
    public function testGetStrength()
    {
        self::assertEquals(60, $this->player->getStrength());
    }
    
    public function testGetDefence()
    {
        self::assertEquals(55, $this->player->getDefence());
    }

    public function testGetSpeed()
    {
        self::assertEquals(65, $this->player->getSpeed());
    }

    public function testGetLuck()
    {
        self::assertEquals(25, $this->player->getLuck());
    }

    public function testGetLuckyChance()
    {
        self::assertEquals(0, $this->player->getLuckyChance());
    }

    public function testIsLucky()
    {
        $this->assertTrue($this->player->isLucky());
    }

    public function testSetIsAttacking()
    {
        $this->player->setIsAttacking(true);
        $this->assertTrue($this->player->isAttacking());
        $this->assertFalse($this->player->isDefending());
    }

    public function testSetIsDefending()
    {
        $this->player->setIsDefending(true);
        $this->assertTrue($this->player->isDefending());
        $this->assertFalse($this->player->isAttacking());
    }

    public function testSetDamage()
    {
        $damage = 10;
        $this->player->setDamage($damage);
        $this->assertSame($damage, $this->player->getDamage());
    }

    public function testAddSkill()
    {
        $skill = $this->createMock(Skill::class);
        $this->player->addSkill($skill);
        $this->assertContains($skill, $this->player->getSkills());
    }

    public function testApplySkills()
    {
        $skill = $this->createMock(Skill::class);
        $skill->expects($this->once())
            ->method('apply')
            ->with($this->player, $this->createMock(Player::class));
        $this->player->addSkill($skill);
        $this->player->applySkills($this->createMock(Player::class), rand(0,100));
    }

    public function testSetUsedSkill()
    {
        $skill = 'rapid_strike';
        $this->player->setUsedSkill($skill);
        $this->assertContains($skill, $this->player->getUsedSkills());
    }

    public function testGetUsedSkills()
    {
        $skill = 'magic_shield';
        $this->player->setUsedSkill($skill);
        $this->assertContains($skill, $this->player->getUsedSkills());
    }

    public function testSetAndGetSkillApplied()
    {
        $skill = $this->createMock(Skill::class);

        $this->player->setSkillApplied($skill);
        $this->assertSame($skill, $this->player->getSkillApplied());
    }

    public function testSetDefaultAttributes()
    {
        $attributes = [
            'health' => [
                'min' => 50,
                'max' => 100
            ],
            'strength' => [
                'min' => 10,
                'max' => 20
            ],
            'defence' => [
                'min' => 5,
                'max' => 10
            ],
            'speed' => [
                'min' => 15,
                'max' => 25
            ],
            'luck' => [
                'min' => 20,
                'max' => 50
            ]
        ];
        $this->player->setDefaultAttributes($attributes);

        $this->assertGreaterThanOrEqual($attributes['health']['min'], $this->player->getHealth());
        $this->assertLessThanOrEqual($attributes['health']['max'], $this->player->getHealth());
        $this->assertGreaterThanOrEqual($attributes['strength']['min'], $this->player->getStrength());
        $this->assertLessThanOrEqual($attributes['strength']['max'], $this->player->getStrength());
        $this->assertGreaterThanOrEqual($attributes['defence']['min'], $this->player->getDefence());
        $this->assertLessThanOrEqual($attributes['defence']['max'], $this->player->getDefence());
        $this->assertGreaterThanOrEqual($attributes['speed']['min'], $this->player->getSpeed());
        $this->assertLessThanOrEqual($attributes['speed']['max'], $this->player->getSpeed());
        $this->assertGreaterThanOrEqual($attributes['luck']['min'], $this->player->getLuck());
        $this->assertLessThanOrEqual($attributes['luck']['max'], $this->player->getLuck());
    }


}