<?php

use Emagia\Skill\MagicShield;
use PHPUnit\Framework\TestCase;
use Emagia\Player\Player;

/**
 * @covers \Emagia\Skill\MagicShield
 */

class MagicShieldTest extends TestCase
{
    protected $skillOwner;
    protected $secondaryPlayer;
    protected $skill;
    
    public function setUp(): void
    {
        //create mock objects for the player class
        $this->skillOwner = $this->createMock(Player::class);
        $this->secondaryPlayer = $this->createMock(Player::class);

        $this->secondaryPlayer->method('getStrength')->willReturn(10);
        $this->skillOwner->method('getDefence')->willReturn(5);
        $this->skillOwner->method('getHealth')->willReturn(100);

        //create an instance of the MagicShield class
        $this->skill = $this->getMockForAbstractClass(MagicShield::class);
    }

    public function testSkillOwnerIsAttacking()
    {
        $this->skillOwner->method('isAttacking')->willReturn(true);
        $this->skillOwner->method('isLucky')->willReturn(false);

        $this->skillOwner->expects($this->never())
            ->method('setHealth');

        $this->skill->apply($this->skillOwner, $this->secondaryPlayer, 0);
    }

    public function testSkillOwnerIsLucky()
    {
        $this->skillOwner->method('isDefending')->willReturn(true);
        $this->skillOwner->method('isLucky')->willReturn(true);

        $this->skillOwner->expects($this->never())
            ->method('setHealth');

        $this->skill->apply($this->skillOwner, $this->secondaryPlayer, 0);
    }

    public function testSkillDoesNotOccur()
    {
        $this->skillOwner->method('isDefending')->willReturn(true);
        //set the attacker's isLucky method to return false
        $this->skillOwner->method('isLucky')->willReturn(false);

        $this->skillOwner->expects($this->never())
            ->method('setHealth');

        $this->skill->apply($this->skillOwner, $this->secondaryPlayer, 100);
    }
    
    public function testSkillGetsApplied()
    {
        $this->skillOwner->method('isDefending')->willReturn(true);
        $this->skillOwner->method('isLucky')->willReturn(false);
        $this->skillOwner->expects($this->once())
            ->method('setHealth')
            ->with(97);

        $this->skillOwner->expects($this->once())
            ->method('setUsedSkill')
            ->with(MagicShield::SKILL_NAME);
        $this->skill->apply($this->skillOwner, $this->secondaryPlayer, 0);
    }
}
