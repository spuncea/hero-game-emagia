<?php

use PHPUnit\Framework\TestCase;
use Emagia\Player\Player;
use Emagia\Skill\RapidStrike;

/**
 * @covers \Emagia\Skill\RapidStrike
 */
class RapidStrikeTest extends TestCase
{
    protected $skillOwner;
    protected $secondaryPlayer;
    protected $skill;

    public function setUp(): void
    {
        //create mock objects for the player class
        $this->skillOwner = $this->createMock(Player::class);
        $this->secondaryPlayer = $this->createMock(Player::class);

        $this->skillOwner->method('getStrength')->willReturn(30);
        $this->secondaryPlayer->method('getDefence')->willReturn(10);
        $this->secondaryPlayer->method('getHealth')->willReturn(100);
        

        //create an instance of the RapidStrike class
        $this->skill = $this->getMockForAbstractClass(RapidStrike::class);
    }

    public function testSkillOwnerIsDefending()
    {
        $this->skillOwner->method('isDefending')->willReturn(true);
        $this->secondaryPlayer->method('isLucky')->willReturn(false);

        $this->secondaryPlayer->expects($this->never())
            ->method('setHealth');

        $this->skill->apply($this->skillOwner, $this->secondaryPlayer, 0);
    }

    public function testSecondaryPlayerIsLucky()
    {
        $this->secondaryPlayer->method('isAttacking')->willReturn(true);
        $this->secondaryPlayer->method('isLucky')->willReturn(true);

        $this->skillOwner->expects($this->never())
            ->method('setHealth');

        $this->skill->apply($this->skillOwner, $this->secondaryPlayer, 0);
    }

    public function testSkillDoesNotOccur()
    {
        $this->skillOwner->method('isAttacking')->willReturn(true);
        //set the attacker's isLucky method to return false
        $this->secondaryPlayer->method('isLucky')->willReturn(false);

        $this->skillOwner->expects($this->never())
            ->method('setHealth');

        $this->skill->apply($this->skillOwner, $this->secondaryPlayer, 100);
    }

    public function testSkillGetsApplied()
    {
        $this->skillOwner->method('isAttacking')->willReturn(true);
        $this->secondaryPlayer->method('isLucky')->willReturn(false);
        $this->secondaryPlayer->expects($this->once())
            ->method('setHealth')
            ->with(60);

        $this->skillOwner->expects($this->once())
            ->method('setUsedSkill')
            ->with(RapidStrike::SKILL_NAME);
        $this->skill->apply($this->skillOwner, $this->secondaryPlayer, 0);
    }
}
