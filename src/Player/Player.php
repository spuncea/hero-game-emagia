<?php
namespace Emagia\Player;

use Emagia\Skill\Skill;

/**
 * Main player class 
 */
class Player implements UsesSkills
{
    /**
     * The name of the player
     * @var string 
     */
    private string $name;
    /**
     * Health Stat
     * @var int 
     */
    private int $health;
    /**
     * Strength Stat
     * @var int
     */
    private int $strength;
    /**
     * Defence Stat
     * @var int
     */
    private int $defence;
    /**
     * Speed Stat
     * @var int
     */
    private int $speed;
    /**
     * Luck Stat
     * @var int
     */
    private int $luck;
    /**
     * Weather is in attack mode
     * @var int
     */
    private bool $isAttacking;
    /**
     * Weather is in defence mode
     * @var bool
     */
    private bool $isDefending;
    /**
     * Damage Stat
     * @var int
     */
    private int $damage;
    /**
     * Available skills to apply
     * @var array
     */
    private array $skills = [];
    /**
     * All the used skills used during the battle
     * @var array
     */
    private array $usedSkills = [];
    /**
     * The skill applied each round if any
     * @var Skill|null
     */
    private Skill|null $skillApplied;
    /**
     * The random chance for being lucky
     * @var int
     */
    private int $luckyChance;

    /**
     * Setter for the name attribute 
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Getter for the name attribute
     * @return string
     */
    public function getName(): string
    {
        return $this->name;      
    }

    /**
     * Setter for the health attribute
     * @param int $health
     */
    public function setHealth(int $health)
    {
        $this->health = $health;
    }

    /**
     * Getter for the health attribute
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * Setter for the strength attribute
     * @param int $strength
     */
    public function setStrength(int $strength)
    {
        $this->strength = $strength;
    }

    /**
     * Getter for the strength attribute
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }

    /**
     * Setter for the defence attribute
     * @param int $defence
     */
    public function setDefence(int $defence)
    {
        $this->defence = $defence;
    }

    /**
     * Getter for the defence attribute 
     * @return int
     */
    public function getDefence(): int
    {
        return $this->defence;
    }

    /**
     * Setter for speed attribute 
     * 
     * @param int $speed
     */
    public function setSpeed(int $speed)
    {
        $this->speed = $speed;
    }

    /**
     * Getter for speed attribute
     * 
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * Setter for luck attribute 
     * 
     * @param int $luck
     */
    public function setLuck(int $luck)
    {
        $this->luck = $luck;
    }

    /**
     * Get the luck attribute 
     * 
     * @return int
     */
    public function getLuck(): int
    {
        return $this->luck;
    }

    /**
     * Check if the player is lucky 
     * 
     * @return bool
     */
    public function isLucky(): bool
    {
        return $this->getLuck() > $this->luckyChance;
    }

    /**
     * Set the random chance of luckiness
     * 
     * @param $chance
     */
    public function setLuckyChance($chance)
    {
        $this->luckyChance = $chance;
    }

    /**
     * Get the random change of luckiness
     * 
     * @return int
     */
    public function getLuckyChance(): int
    {
        return $this->luckyChance;
    }

    /**
     * Set current player as attacking 
     * 
     * @param bool $flag
     */
    public function setIsAttacking(bool $flag)
    {
        $this->isAttacking = $flag;
        $this->isDefending = !$flag;
    }

    /**
     * Check if current player is attacking
     * 
     * @return bool
     */
    public function isAttacking(): bool
    {
        return !!$this->isAttacking;
    }

    /**
     * Set the current player as defending
     * @param bool $flag
     */
    public function setIsDefending(bool $flag)
    {
        $this->isDefending = $flag;
        $this->isAttacking = !$flag;
    }

    /**
     * Check if current player is defending 
     * 
     * @return bool
     */
    public function isDefending(): bool
    {
        return !!$this->isDefending;
    }

    /**
     * Setter for the damage attribute 
     * 
     * @param int $damage
     */
    public function setDamage(int $damage)
    {
        $this->damage = $damage;
    }

    /**
     * Getter for the damage attribute 
     * 
     * @return int
     */
    public function getDamage(): int
    {
        return $this->damage;        
    }

    /**
     * Attach a skill to the list of available skills 
     * 
     * @param Skill $skill
     */
    public function addSkill(Skill $skill): void
    {
        $this->skills[] = $skill;
    }

    /**
     * Get available skills 
     * 
     * @return array[]
     */
    public function getSkills(): array
    {
        return $this->skills;
    }

    /**
     * Apply available skills to current player 
     * 
     * @param Player $secondaryPlayer This is the other player 
     * @param int $randomChance This is the chance of applying the skill 
     */
    public function applySkills(Player $secondaryPlayer, $randomChance): void
    {
        foreach ($this->skills as $skill) {
            $skill->apply($this, $secondaryPlayer, $randomChance);
        }
    }

    /**
     * Add the used skill to the list
     * 
     * @param string $skill
     */
    public function setUsedSkill(string $skill): void
    {
        if(!in_array($skill, $this->usedSkills)) {
            $this->usedSkills[] = $skill;
        }
    }

    /**
     * Get a list of used skills during the battle 
     * 
     * @return array
     */
    public function getUsedSkills(): array
    {
        return $this->usedSkills;
    }

    /**
     * Set the skill that was applied during the round
     * 
     * @param Skill|null $skill
     */
    public function setSkillApplied(?Skill $skill)
    {
        $this->skillApplied = $skill;
    }

    /**
     * Get the skill that was applied during the round 
     * @return Skill|null
     */
    public function getSkillApplied(): ?Skill
    {
        return $this->skillApplied;
    }

    /**
     * Set the default attributes of the player
     * 
     * @param array $attributes
     */
    public function setDefaultAttributes(array $attributes)
    {
        foreach($attributes as $attribute => $value) {
            $methodName = 'set' . ucfirst($attribute);
            if (method_exists($this, $methodName) && array_key_exists('min', $value) && array_key_exists('max', $value)) {
                $this->{$methodName}(mt_rand($value['min'], $value['max']));
            }
        }
    }
}
