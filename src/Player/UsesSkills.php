<?php

namespace Emagia\Player;

use Emagia\Skill\Skill;

interface UsesSkills
{
    /**
     * Add specific skill as being available
     * 
     * @param Skill $skill
     * @return void
     */
    public function addSkill(Skill $skill): void;

    /**
     * Returns an array of available skills 
     * 
     * @return array
     */
    public function getSkills(): array;

    /**
     * Apply the skills in accordance with the secondary player
     * Give also a random chance of applying 
     * 
     * @param Player $secondaryPlayer
     * @param $randomChance
     * @return void
     */
    public function applySkills(Player $secondaryPlayer, $randomChance): void;

    /**
     * Set the unique skill as used 
     * 
     * @param string $skill
     * @return void
     */
    public function setUsedSkill(string $skill): void;

    /**
     * Get all used skills 
     * 
     * @return array
     */
    public function getUsedSkills(): array;

    /**
     * Remember what skill was used each round 
     * 
     * @param Skill|null $skill
     * @return void
     */
    public function setSkillApplied(?Skill $skill);

    /**
     * Get the skill used in the round if any
     * 
     * @return Skill|null
     */
    public function getSkillApplied(): ?Skill;
   
}