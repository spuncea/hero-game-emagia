<?php

namespace Emagia;

use Emagia\Player\Player;
use Emagia\Skill\MagicShield;
use Emagia\Skill\RapidStrike;
use Emagia\Utils\TerminalColors;

class BattleGroundHelper
{
    use TerminalColors;

    /**
     * Establish who is the attacker and who is the defender 
     * 
     * @param Player $hero
     * @param Player $beast
     */
    public function chooseSides(Player $hero, Player $beast)
    {
        if($hero->getSpeed() === $beast->getSpeed()) {
            $hero->setIsAttacking($hero->getLuck() > $beast->getLuck());
            $beast->setIsAttacking($hero->getLuck() < $beast->getLuck());
        }
        else {
            $hero->setIsAttacking($hero->getSpeed() > $beast->getSpeed());
            $beast->setIsAttacking($hero->getSpeed() < $beast->getSpeed());
        }
    }

    /**
     * Carries the Attack
     * 
     * @param Player $hero
     * @param Player $beast
     * @return boolean
     */
    public function attack(Player $hero, Player $beast): bool
    {
        // remove any previous skill used 
        $hero->setSkillApplied(null);

        // first use the skills available with a random chance of occurrence
        $hero->applySkills($beast, rand(0,100));
        
        // if hero is attacking and no skill was applied then continue to inflict damage 
        if($hero->isAttacking() && !$hero->getSkillApplied()) {
            // reset the damage each time
            $hero->setDamage(0);
            $beast->setDamage(0);
            
            // calculate new damage to inflict
            $damage = $hero->getStrength() - $beast->getDefence();
            
            // continue to damage the beast
            if(!$beast->isLucky()){
                $beast->setHealth($beast->getHealth() - $damage);
                $hero->setDamage($damage);
            }
        }
        
        if($hero->isDefending() && !$hero->getSkillApplied()) {
            // chance of getting lucky
            $hero->setDamage(0);
            $beast->setDamage(0);
            
            $damage = $beast->getStrength() - $hero->getDefence();
            // continue to damage the hero
            if(!$hero->isLucky()){
                $hero->setHealth($hero->getHealth() - $damage);
                $beast->setDamage($damage);
            }
        } 
        
        if($hero->getHealth() <= 0) {
            $hero->setHealth(0);
            return true;
        }

        if($beast->getHealth() <= 0) {
            $beast->setHealth(0);
            return true;
        }

        return false;
    }

    /**
     * The attacker becomes the defender and vice-versa
     * @param Player $hero
     * @param Player $beast
     */
    public function switchSides(Player $hero, Player $beast)
    {
        $hero->setIsAttacking($hero->isDefending());
        $beast->setIsAttacking($beast->isDefending());
    }
    

    /**
     * Show the starting attributes
     * 
     * @param Player $hero
     * @param Player $beast
     */
    public function showStartUpAttributes(Player $hero, Player $beast)
    {
        $heroStance = $hero->isAttacking() ? 'ATTACKER' : 'DEFENDER';
        $beastStance = $beast->isAttacking() ? 'ATTACKER' : 'DEFENDER';

        echo PHP_EOL;
        echo "|STARTING ATTRIBUTES" . PHP_EOL;
        echo "|-------------------" . PHP_EOL;
        echo "| HERO - " . $heroStance . ":  HT:" . ($this->getHealthColor($hero->getHealth()) . " | ST: " . $hero->getStrength()) . " | DF: " 
                . $hero->getDefence() . " | SP: " .  $hero->getSpeed() . " | LU:" . $hero->getLuck() . PHP_EOL;
        echo "| BEAST - " . $beastStance .  ": HT:" . ($this->getHealthColor($beast->getHealth()) . " | ST: " . $beast->getStrength()) . " | DF: "
            . $beast->getDefence() . " | SP: " .  $beast->getSpeed() . " | LU:" . $beast->getLuck();
        
    }
    
    public function getHealthColor($percentage)
    {
        if ($percentage > 75) {
            $colored = $this->greenText($percentage . "%");
        } elseif ($percentage > 25) {
            $colored =  $this->orangeText($percentage . "%");
        } else {
            $colored = $this->redText($percentage . "%");
        }
        
        return rtrim($colored);
        
    } 


    /**
     * Show each round details
     * @param Player $hero
     * @param Player $beast
     * @param int $turn
     */
    public function showBattleRoundAttributes(Player $hero, Player $beast, $turn)
    {
        $heroType = $hero->isAttacking() ? "ATTACKER" : "DEFENDER";
        $beastType = $beast->isAttacking() ? "ATTACKER" : "DEFENDER";

        $defender = $hero->isDefending() ? "HERO" : "BEAST";
        
        $isHeroLucky = $hero->isLucky();
        $isBeastLucky = $beast->isLucky();
        
        $wasLuckyMessage = '';
        
        if ($hero->isDefending() && $isHeroLucky || $beast->isDefending() && $isBeastLucky) {
            $wasLuckyMessage .= $defender . ' - GOT LUCKY THIS ROUND - NO DAMAGE ';
        }
        
        echo PHP_EOL;
        echo "|                              ROUND " . $turn . PHP_EOL;
        echo "|" . ($hero->getSkillApplied() instanceof MagicShield ? 'used magic shield' :( $hero->getSkillApplied() instanceof RapidStrike ? 'used rapid strike' : 'no skills used')) . PHP_EOL;
        echo "|" . $wasLuckyMessage . PHP_EOL;
       
        echo "| ----------------------------------------------------------------" . PHP_EOL;
        echo "|                           HERO | BEAST " . PHP_EOL;
        echo "| Health:                   " . $hero->getHealth() . " % " . "| " . $beast->getHealth() . " % " . PHP_EOL;
        echo "| Strength:                 " . $hero->getStrength() . " % " . "| " . $beast->getStrength() . " % " . PHP_EOL;
        echo "| Defence:                  " . $hero->getDefence() . " % " . "| " . $beast->getDefence() . " % " . PHP_EOL;
        echo "| Speed:                    " . $hero->getSpeed() . " % " . "| " . $beast->getSpeed() . " % " . PHP_EOL;
        echo "| Luck:                     " . $hero->getLuck() . " % " . "| " . $beast->getLuck() . " % " . PHP_EOL;
        echo "|                           " . $heroType . " | " . $beastType . PHP_EOL;
    }

    /**
     * Show on the terminal the winner of the fight and some stats.
     * @param Player $hero
     * @param Player $beast
     */
    public function showWinner(Player $hero, Player $beast)
    {
        $winner = $this->getWinner($hero, $beast);
        
        $usedRapidStrike = in_array('RAPID_STRIKE',$hero->getUsedSkills()) ? "^ YES ^" : "NO";
        $usedMagicShield = in_array('MAGIC_SHIELD', $hero->getUsedSkills()) ? "^ YES ^" : "NO";

        echo PHP_EOL;
        echo "|-------------------------------------" . PHP_EOL;
        echo "| WINNER OF THE FIGHT IS: * " . $winner->getName() . " * " . PHP_EOL ;
        echo "| HERO USED SKILL RAPID STRIKE: " . $usedRapidStrike . PHP_EOL ;
        echo "| HERO USED SKILL MAGIC SHIELD: " . $usedMagicShield . PHP_EOL ;
    }

    /**
     * Return the winner
     * @param Player $hero
     * @param Player $beast
     * @return Player
     */
    public function getWinner(Player $hero, Player $beast): Player
    {
        if ($hero->getHealth() < $beast->getHealth()) {
            return $beast;
        }elseif ($hero->getHealth() > $beast->getHealth()) {
            return $hero;
        }else{
            return $hero->getStrength() > $beast->getStrength() ? $hero : $beast;
        }
        
    }
}