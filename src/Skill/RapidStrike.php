<?php

namespace Emagia\Skill;

use Emagia\Player\Player;

/**
 * Special Skill
 * Will occur only in these circumstances:
 * - $skillOwner is attacking
 * - $secondaryPlayer is not lucky
 * - $randomChance is smaller than the chance of this skill occurrence chance
 */
class RapidStrike implements Skill
{
    const SKILL_NAME = 'RAPID_STRIKE';

    /**
     * @var int Chance of using this skill
     */
    private int $chance = 10;

    /**
     * The skill owner applies this skill 
     * 
     * @param Player $skillOwner
     * @param Player $secondaryPlayer
     * @param $randomChance
     */
    public function apply(Player $skillOwner, Player $secondaryPlayer, $randomChance)
    {
        // this skill applies only when the skill owner is attacking
        if($skillOwner->isDefending()) {
            return;
        }
        
        // if the other player is lucky no damage will be dealt
        if($secondaryPlayer->isLucky()) {
            return;
        }
        
        // if the chance does not occur skill will not happen
        if($randomChance > $this->chance) {
            return;
        }
        
        // inflict twice the damage
        $damage = ($skillOwner->getStrength() - $secondaryPlayer->getDefence()) * 2;
        $secondaryPlayer->setHealth($secondaryPlayer->getHealth() - $damage);
        $skillOwner->setDamage($damage);
        $skillOwner->setSkillApplied($this);
        $skillOwner->setUsedSkill(static::SKILL_NAME);
        
    }
}