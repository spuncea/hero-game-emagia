<?php

namespace Emagia\Skill;

use Emagia\Player\Player;

interface Skill
{
    /**
     * Apply a specific skill to the skill owner with a random chance of triggering the skill
     * 
     * @param Player $skillOwner
     * @param Player $secondaryPlayer
     * @param $randomChance
     * @return void
     */
    public function apply(Player $skillOwner, Player $secondaryPlayer, $randomChance);
}