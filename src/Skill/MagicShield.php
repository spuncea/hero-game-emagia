<?php

namespace Emagia\Skill;

use Emagia\Skill\Skill;
use Emagia\Player\Player;

/**
 * Special Skill 
 * Will occur only in these circumstances:
 * - $skillOwner is defending this time 
 * - $skillOwner is not lucky this time
 * - $randomChance is smaller than the chance of this skill occurrence chance 
 */
class MagicShield implements Skill
{
    const SKILL_NAME = 'MAGIC_SHIELD';
    
    private int $chance = 20;

    /**
     * @param Player $skillOwner
     * @param Player $secondaryPlayer
     * @param $randomChance
     */
    public function apply(Player $skillOwner, Player $secondaryPlayer, $randomChance)
    {
        // this skill applies only when the skill owner is defending
        if($skillOwner->isAttacking()) {
            return;
        }
        // if the skill owner is lucky no damage will be dealt to him
        if($skillOwner->isLucky()) {
            return;
        }
        
        // if the chance does not occur skill does not happen
        if($randomChance > $this->chance) {
            return;
        }
        
        // inflict half of damage 
        $damage = ($secondaryPlayer->getStrength() - $skillOwner->getDefence()) / 2;
        $skillOwner->setHealth($skillOwner->getHealth() - $damage);
        $secondaryPlayer->setDamage($damage);
        $skillOwner->setSkillApplied($this);
        $skillOwner->setUsedSkill(static::SKILL_NAME);
    }
}