<?php

namespace Emagia;

use Emagia\Skill\MagicShield;
use Emagia\Player\Player;

use Emagia\Skill\RapidStrike;
use Emagia\Utils\Config;

class BattleGround extends BattleGroundHelper
{
    private Player $orderus;
    private Player $beast;

    public function __construct()
    {
        $this->orderus = new Player();
        $this->orderus->setName('Orderus');
        $this->orderus->setDefaultAttributes(Config::HERO_ATT);
        $this->orderus->addSkill(new RapidStrike());
        $this->orderus->addSkill(new MagicShield());
        
        $this->beast = new Player();
        $this->beast->setName('Wild Beast');
        $this->beast->setDefaultAttributes(Config::ANIMAL_ATT);
    }

    /**
     * Start the battle
     */
    public function start()
    {
        // choose who is attacking and who is defending 
        $this->chooseSides($this->orderus, $this->beast);
        
        $this->showStartUpAttributes($this->orderus, $this->beast);
        
        for($turn = 1; $turn <= Config::ROUNDS; $turn++) {
            
            // initialize the lucky chance for each player each turn 
            $this->orderus->setLuckyChance(rand(0,100));
            $this->beast->setLuckyChance(rand(0,100));

            $battleEnded = $this->attack($this->orderus, $this->beast);
            $this->showBattleRoundAttributes($this->orderus, $this->beast, $turn);

            // after each turn switching sides
            $this->switchSides($this->orderus, $this->beast);

            // if the fight is over before the total number of rounds
            if($battleEnded) {
                $this->showWinner($this->orderus, $this->beast);
                return;
            }
        }
        

        // if the battle did not end after the number of turns automatically show the winner.
        $this->showWinner($this->orderus, $this->beast);
    }
    
}