<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Emagia\Utils;

/**
 * Description of Config
 *
 * @author sorin
 */
final class Config {
    
    /**
     * Number of rounds until finish
     */
    const ROUNDS = 20;
    
    /**
     * Default HERO attributes
     */
    const HERO_ATT = [
        'health'    => ['min' => 70, 'max' => 100],
        'strength'  => ['min' => 70, 'max' => 80],
        'speed'     => ['min' => 40, 'max' => 50],
        'defence'   => ['min' => 45, 'max' => 55],
        'luck'      => ['min' => 10, 'max' => 30]
    ];

    /**
     * Default ANIMAL attributes
     */
    const ANIMAL_ATT = [
        'health'    => ['min' => 60, 'max' => 90],
        'strength'  => ['min' => 60, 'max' => 90],
        'speed'     => ['min' => 40, 'max' => 60],
        'defence'   => ['min' => 40, 'max' => 60],
        'luck'      => ['min' => 25, 'max' => 40]
    ];
    
    
}
