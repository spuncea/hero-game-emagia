<?php

namespace Emagia\Utils;

trait TerminalColors
{
    public function greenText(string $text): string
    {
        return rtrim("\033[32m" . $text . "\033[0m\n");
    }

    public function orangeText(string $text): string
    {
        return rtrim("\033[33m" . $text . "\033[0m\n");
    }

    public function redText(string $text): string
    {
        return rtrim("\033[31m" . $text . "\033[0m\n");
    }

    public function flashingRedText(string $text): string
    {
        return "\033[5;31m". $text ."\033[0m";        
    }
}